# public_static_void_x2

*"Gedanken ohne Inhalt sind leer, Anschauungen ohne Begriffe sind blind." Immanuel Kant, Einleitung zur Idee einer Transzendentalen Logik*

* https://www.cospaces.io/
* https://www.csunplugged.org/de/
* https://troeger.eu/unplugged/

[Ka97]  Kant, I.: Kritik der reinen Vernunft 1. Herausgegeben von Wilhelm Weischedel, suhrkamp taschenbuch wissenschaft, Insel Verlag Wiesbaden, 1.-3. Auflage, 1997, S. 98.
